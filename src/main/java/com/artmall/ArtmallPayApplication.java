package com.artmall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArtmallPayApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArtmallPayApplication.class, args);
    }
}
