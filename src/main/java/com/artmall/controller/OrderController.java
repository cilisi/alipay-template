package com.artmall.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.artmall.bean.AlipayBean;
import com.artmall.bean.AlipayResponseBean;
import com.artmall.config.AlipayUtil;
import com.artmall.config.PropertiesConfig;
import com.artmall.service.PayService;
import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.ArrayList;

import static com.artmall.config.AlipayUtil.cheackSign;

/* 订单接口 */
@Controller
public class OrderController {
    @Autowired
    private PayService payService;//调用支付服务

    /**
     *  支付宝支付请求页面
     * @param outTradeNo
     * @param subject
     * @param totalAmount
     * @return
     * @throws AlipayApiException
     */
    /*阿里支付*/
    @RequestMapping("/alipay")
    @ResponseBody
    public String alipayPay(@RequestBody AlipayBean bean) throws AlipayApiException {
        return payService.pay(bean);
    }

    /**
     *
     * @param alipay_trade_wap_pay_response
     * @return
     */
    @RequestMapping(value = "/alipay/synchronized/notic",method = RequestMethod.GET)
     @ResponseBody
    public String alipaySynchronizedResponse(@RequestBody AlipayResponseBean alipayResponseBean){
        Boolean b=cheackSign(alipayResponseBean.getClass());
        //验签成功
        if(b){//验证成功
            //////////////////////////////////////////////////////////////////////////////////////////
            //请在这里加上商户的业务逻辑程序代码

            //////////////////////////////////////////////////////////////////////////////////////////
        }else{

        }
        return "";//支付宝返回跳转通知页面
    }
    /**
     *
      * @param alipay_trade_wap_pay_response
     * @return
     */
    @RequestMapping(value = "/alipay/notify/notic",method = RequestMethod.POST)
    @ResponseBody
    public String alipayNotifylResponse(@RequestBody AlipayResponseBean alipayResponseBean) {
        Boolean b=cheackSign(alipayResponseBean.getClass());
        //验签成功
        if(b){
            String trade_status=alipayResponseBean.getSub_code();
            //订单完成
            if(trade_status.equals("TRADE_FINISHED")){

                /**
                 * 业务逻辑
                 */
                //订单成功
            } else if (trade_status.equals("TRADE_SUCCESS")){
                /**
                 *业务逻辑
                 */
                //订单关闭
            } else if(trade_status.equals("TRADE_CLOSED"));
        }
        return "success";//一定要传回success
    }

    /**
     * 交易查询
     * @param out_trade_no  商户网站唯一订单号
     * @param trade_no 这笔交易在支付宝系统中的交易流水号，最长64位
     * @return
     */
    @RequestMapping("/qurey")
    @ResponseBody
    public String alipayQurey(@RequestParam("out_trade_no") String out_trade_no,@RequestParam("trade_no")String trade_no){
        return payService.query(out_trade_no,trade_no);
    }
    /**
     * 交易关闭查询
     * @param out_trade_no  商户网站唯一订单号
     * @param trade_no 这笔交易在支付宝系统中的交易流水号，最长64位
     * @return
     */
    @RequestMapping("/close")
    @ResponseBody
    public String alipayClose(@RequestParam("out_trade_no") String out_trade_no,@RequestParam("trade_no")String trade_no){
        return payService.close(out_trade_no,trade_no);
    }

    /**
     * 账单下载，默认trade,月账单格式，当bill_type设置为1时，为signcustomer，bill_date设置为1时，为日账单格式
     * @param bill_type // "trade"指商户基于支付宝交易收单的业务账单,"signcustomer"是指基于商户支付宝余额收入及支出等资金变动的帐务账单；
     * @param bill_date // 账单时间：日账单格式为yyyy-MM-dd，月账单格式为yyyy-MM
     * @return
     */
    @RequestMapping("/downbill")
    @ResponseBody
    public String alipayDownBill(@RequestParam("bill_type")@Nullable String bill_type, @RequestParam("bill_date")@Nullable String bill_date){
        String bill_typeS="trade",bill_dateS="yyyy-MM";
        if(bill_type.equals("1")) bill_typeS="signcustomer";
        if(bill_date.equals("1")) bill_dateS="yyyy-MM-dd";
        return payService.downBill(bill_typeS,bill_dateS);
    }
}
