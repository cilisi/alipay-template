package com.artmall.bean;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class AlipayResponseBean{

    /**
     * 必填
     */
    /*网关返回码*/
    private String code;
    /*网关返回描述*/
    private String msg;
    /*签名*/
    private String sign;
    /*商户网站唯一订单号*/
    private String out_trade_no;
    /*这笔交易在支付宝系统中的交易流水号，最长64位*/
    private String trade_no;
    /*收款支付账号对应的支付宝账号，2088开头*/
    private String seller_id;
    /*该笔订单的资金总额，rmb范围[0.01-100000000.00]，精确到小数点后两位*/
    private String total_amount;
    /**
     * 选填
     */
    /*业务返回码*/
    private String sub_code;
    /*业务返回码描述*/
    private String sub_msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public String getTrade_no() {
        return trade_no;
    }

    public void setTrade_no(String trade_no) {
        this.trade_no = trade_no;
    }

    public String getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(String seller_id) {
        this.seller_id = seller_id;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getSub_code() {
        return sub_code;
    }

    public void setSub_code(String sub_code) {
        this.sub_code = sub_code;
    }

    public String getSub_msg() {
        return sub_msg;
    }

    public void setSub_msg(String sub_msg) {
        this.sub_msg = sub_msg;
    }
}
