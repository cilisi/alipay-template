package com.artmall.bean;

import org.springframework.stereotype.Repository;

/*支付实体对象*/
@Repository
final public class AlipayBean{
    /**
     * 必填
     */
    /*商户订单号*/
    private String out_trade_no;
    /*订单名称*/
    private String subject;
    /*付款金额*/
    private StringBuffer total_amount;
    /**
     * 可空
     */
   /*商品描述*/
   private String body;
   /*商品主类型 :0-虚拟类商品,1-实物类商品*/
   private String goods_type;
   /*用户付款中途退出返回商户网站的地址*/
   private String quit_url;

   /*超时时间参数*/
   private String timeout_express="10m";
   private String product_code="FAST_INSTANT_TRADE_PAY";

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public StringBuffer getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(StringBuffer total_amount) {
        this.total_amount = total_amount;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getGoods_type() {
        return goods_type;
    }

    public void setGoods_type(String goods_type) {
        this.goods_type = goods_type;
    }

    public String getQuit_url() {
        return quit_url;
    }

    public void setQuit_url(String quit_url) {
        this.quit_url = quit_url;
    }

    public String getTimeout_express() {
        return timeout_express;
    }

    public void setTimeout_express(String timeout_express) {
        this.timeout_express = timeout_express;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }
}
